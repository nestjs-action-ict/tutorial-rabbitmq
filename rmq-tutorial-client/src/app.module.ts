import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Transport, ClientsModule } from '@nestjs/microservices';
import {environment} from "./conf/config";
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'RMQ_SERVICE', transport: Transport.RMQ,
        options: {
          urls: [`amqp://${environment.USER}:${environment.PWD}@localhost:5672/${environment.VIRTUAL_HOST}`],
          queue: environment.QUEUE,
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
  controllers: [AppController],
  providers: [AppService] })
export class AppModule {
}
