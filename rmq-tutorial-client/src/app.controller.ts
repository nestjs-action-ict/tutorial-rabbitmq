import { Controller, Get, Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { MessageEvent } from './message.event';

@Controller()
export class AppController {
  constructor(@Inject('RMQ_SERVICE') private readonly client: ClientProxy) { }

  async onApplicationBootstrap() {
    await this.client.connect();
  }

  @Get()
  getHello() {
    this.client.emit<any>('message_printed', new MessageEvent('Hello World'));
    return 'Hello World printed';
  }
}
