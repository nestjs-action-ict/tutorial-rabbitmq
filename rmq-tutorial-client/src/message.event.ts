export class MessageEvent {
    text: string;

    constructor(text) {
        this.text = text;
    }
}
