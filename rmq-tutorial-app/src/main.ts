import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';
import {environment} from "./conf/config";

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: [`amqp://${environment.USER}:${environment.PWD}@localhost:5672/${environment.VIRTUAL_HOST}`],
      queue: environment.QUEUE,
      queueOptions: {
        durable: false,
      },
    },
  });
  await app.listen(() => console.log('Microservice is listening'));
}
bootstrap();
