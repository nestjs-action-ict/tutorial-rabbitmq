export const environment = {
    USER: 'guest',
    PWD: 'guest',
    VIRTUAL_HOST: 'tutorial-host',
    RABBITMQ_ADDRESS: 'localhost:5672',
    QUEUE: 'user-messages'
}
